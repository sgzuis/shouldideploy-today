<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Yasumi\Yasumi;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('', static function (Request $request) {
    $timezone = $request->get('timezone', 'UTC');

    if (!in_array($timezone, DateTimeZone::listIdentifiers(), true)) {
        dd($timezone);
    }

    $timezone = new DateTimeZone($timezone);

    $now = now($timezone);

    $shouldideploy = false;

    if (!$now->isFriday() && !$now->isWeekend()) {
        $shouldideploy = true;
    }

    $reason = 'REASONS_TO_DEPLOY';

    if ($now->isDayBeforeChristmas()) {
        $reason = 'REASONS_FOR_DAY_BEFORE_CHRISTMAS';
    }

    if ($now->isChristmas()) {
        $reason = 'REASONS_FOR_CHRISTMAS';
    }

    if ($now->isNewYear()) {
        $reason = 'REASONS_NEW_YEAR';
    }

    if ($now->isFriday13th()) {
        $reason = 'REASONS_FOR_FRIDAY_13TH';
    }

    if ($now->isFridayAfternoon()) {
        $reason = 'REASONS_FOR_FRIDAY_AFTERNOON';
    }

    if ($now->isFriday()) {
        $reason = 'REASONS_TO_NOT_DEPLOY';
    }
    if ($now->isThursdayAfternoon()) {
        $reason = 'REASONS_FOR_THURSDAY_AFTERNOON';
    }

    if ($now->isAfternoon() && !$now->isWeekend()) {
        $reason = 'REASONS_FOR_AFTERNOON';
    }

    if ($now->isWeekend()) {
        $reason = 'REASONS_FOR_WEEKEND';
    }

    $reasons = config('reasons.' . $reason);

    $message = $reasons[array_rand($reasons)];

    return response()->json([
        'timezone' => $timezone->getName(),
        'shouldideploy' => $shouldideploy,
        'message' => $message,
    ]);
});
