<?php
$REASONS_FOR_DAY_BEFORE_CHRISTMAS = [
    'Are you Santa 🧑‍🎄 or what?',
    '🎶🎵 You better watch out 🎵🎶',
    '🎄 Enjoy the holiday season! 🎄 ',
    'Just take another glass of eggnog',
    "Can't you just wait after present unwrapping?",
    'Sure, deploy... \n your family will appreciate you fixing things on your phone during dinner'
];

$REASONS_FOR_CHRISTMAS = [
    ...$REASONS_FOR_DAY_BEFORE_CHRISTMAS,
    'No, Rudolf will hunt you down 🦌 ',
    'Just watch Home Alone today',
    "Shouldn't you be preparing a christmas diner?"
];

$REASONS_NEW_YEAR = [
    'Happy New Year! \n deploy the 2nd of january',
    "Aren't you hungover?",
    'Take another glass of champagne 🥂',
    'Celebrate today, deploy tomorrow 🎇'
];

$REASONS_FOR_FRIDAY_13TH = [
    "Man, really? It's friday the 13th!",
    'Do you believe in bad luck?',
    'Jason is watching you',
    'If you want to spend your weekend in Crystal Lake, go ahead',
    'To pray is no help if you take this bad decision',
    'Did you look at the calendar today?',
    '📅 Friday the 13th. What do you think about it?',
    'Just no!',
    'But but but... why?'
];

$REASONS_FOR_FRIDAY_AFTERNOON = [
    'Not by any chance',
    'U mad?',
    'What you are thinking?',
    'No no no no no no no no',
    'How do you feel about working nights and weekends?',
    '🔥 🚒 🚨 ⛔️ 🔥 🚒 🚨 ⛔️ 🔥 🚒 🚨 ⛔️',
    'No! God! Please! No',
    'No no no no no no no!',
    'Keep dreaming darling',
    'Why why Bro why?',
    'But but but... why?',
    'Deploys are for Monday, so you can fix them till Friday.',
    'YOLO ! You only live once !'
];

$REASONS_TO_NOT_DEPLOY = [
    "I wouldn't recommend it",
    "No, it's Friday",
    'What about Monday?',
    'Not today',
    'Nope',
    'Why?',
    'Did the tests pass? Probably not',
    '¯\\_(ツ)_/¯',
    '😹',
    'No',
    'No. Breathe and count to 10, start again',
    "I'd rather have ice-cream 🍦",
    'How could you? 🥺'
];

$REASONS_FOR_THURSDAY_AFTERNOON = [
    'You still want to sleep?',
    'Call your partner!',
    'Gonna stay late today?',
    'Tell your boss that you found a bug and go home',
    'What about Monday?',
    "I wouldn't recommend it",
    'Not today',
    'Nope',
    'No. Breathe and count to 10, start again'
];

$REASONS_FOR_AFTERNOON = [
    'You still want to sleep?',
    'Call your partner!',
    'Gonna stay late today?',
    'Tomorrow?',
    'No',
    'Tell your boss that you found a bug and go home',
    'You have full day ahead of you tomorrow!',
    "Trust me, they will be much happier if it wasn't broken for a night",
    'How much do you trust your logging tools?'
];

$REASONS_FOR_WEEKEND = [
    "Go home, you're drunk",
    'How about Monday?',
    'Beer?',
    'Drunk development is not a good idea!',
    'I see you deployed on Friday',
    'Told you that Monday would be a better idea!'
];

$REASONS_TO_DEPLOY = [
    "I don't see why not",
    "It's a free country",
    'Go ahead my friend!',
    'Go for it',
    'Go go go go!',
    "Let's do it!",
    'Ship it! 🚢',
    'Go with the flow 🌊',
    'Harder better faster stronger',
    'Rock on!',
    'Make me proud',
    'Break a leg!',
    'This Is the Way',
    'Strike First, Strike Hard, No Mercy!'
];

return [
    'REASONS_FOR_DAY_BEFORE_CHRISTMAS' => $REASONS_FOR_DAY_BEFORE_CHRISTMAS,
    'REASONS_FOR_CHRISTMAS' => $REASONS_FOR_CHRISTMAS,
    'REASONS_NEW_YEAR' => $REASONS_NEW_YEAR,
    'REASONS_FOR_FRIDAY_13TH' => $REASONS_FOR_FRIDAY_13TH,
    'REASONS_FOR_FRIDAY_AFTERNOON' => $REASONS_FOR_FRIDAY_AFTERNOON,
    'REASONS_TO_NOT_DEPLOY' => $REASONS_TO_NOT_DEPLOY,
    'REASONS_FOR_THURSDAY_AFTERNOON' => $REASONS_FOR_THURSDAY_AFTERNOON,
    'REASONS_FOR_AFTERNOON' => $REASONS_FOR_AFTERNOON,
    'REASONS_FOR_WEEKEND' => $REASONS_FOR_WEEKEND,
    'REASONS_TO_DEPLOY' => $REASONS_TO_DEPLOY,
];
