<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class CarbonServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::macro('isAfternoon', static function () {
            return self::this()->hour >= 16;
        });

        Carbon::macro('isThursday', static function () {
            return self::this()->weekday() === self::THURSDAY;
        });

        Carbon::macro('isThursdayAfternoon', static function () {
            return self::this()->isThursday() && self::this()->isAfternoon();
        });

        Carbon::macro('isFriday', static function () {
            return self::this()->weekday() === self::FRIDAY;
        });

        Carbon::macro('isFridayAfternoon', static function () {
            return self::this()->isFriday() && self::this()->isAfternoon();
        });

        Carbon::macro('isFriday13th', static function () {
            return self::this()->isFriday() && self::this()->day === 13;
        });

        Carbon::macro('isDayBeforeChristmas', static function () {
            return (
                self::this()->month === 12 &&
                self::this()->day === 24 &&
                self::this()->hour >= 16
            );
        });

        Carbon::macro('isChristmas', static function () {
            return self::this()->month === 12 && self::this()->day === 25;
        });

        Carbon::macro('isNewYear', static function () {
            return (
                (self::this()->month === 12 &&
                    self::this()->day === 31 &&
                    self::this()->hour >= 16) ||
                (self::this()->month === 0 && self::this()->day === 1)
            );
        });

        Carbon::macro('isHolidays', static function () {
            return self::this()->isDayBeforeChristmas() || self::this()->isChristmas() || self::this()->isNewYear();
        });
    }
}
